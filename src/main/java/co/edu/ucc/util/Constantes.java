package co.edu.ucc.util;

public class Constantes {

	private Constantes() {
	}

	public static final int CODIGO_ACC_READ = 1;
	public static final int CODIGO_ACC_WRITE = 2;
	public static final int CODIGO_ACC_READ_WRITE = 3;
	public static final int CODIGO_ACC_SUSCRIPTION = 4;

}
