package co.edu.ucc.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@SuppressWarnings("serial")
public class AclMosquitto extends CamposComunesdeEntidad {

	@ManyToOne
	private UserMosquitto userMosquitto;
	private String topic;
	private Boolean readPermission;
	private Boolean writePermission;
	private Boolean suscriptionPermission;

	public UserMosquitto getUserMosquitto() {
		return userMosquitto;
	}

	public void setUserMosquitto(UserMosquitto userMosquitto) {
		this.userMosquitto = userMosquitto;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public Boolean getReadPermission() {
		return readPermission;
	}

	public void setReadPermission(Boolean readPermission) {
		this.readPermission = readPermission;
	}

	public Boolean getWritePermission() {
		return writePermission;
	}

	public void setWritePermission(Boolean writePermission) {
		this.writePermission = writePermission;
	}

	public Boolean getSuscriptionPermission() {
		return suscriptionPermission;
	}

	public void setSuscriptionPermission(Boolean suscriptionPermission) {
		this.suscriptionPermission = suscriptionPermission;
	}

}
