package co.edu.ucc.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import co.edu.ucc.dao.IAclMosquittoDAO;
import co.edu.ucc.dao.IUserMosquittoDAO;
import co.edu.ucc.entity.AclMosquitto;
import co.edu.ucc.entity.UserMosquitto;
import co.edu.ucc.exception.ExceptionUserAuthentication;
import co.edu.ucc.model.AuthorizationAclDTO;
import co.edu.ucc.model.UserDTO;
import co.edu.ucc.util.Constantes;
import co.edu.ucc.util.utils;

@Service
public class AuthenticateMqttService {

	@Autowired
	private IUserMosquittoDAO userMosquittoDAO;
	@Autowired
	private IAclMosquittoDAO aclMosquittoDAO;
	
	@Value("${mqtt.key_secret}")
	private String key;

	private Logger logger = LogManager.getLogger(AuthenticateMqttService.class);

	public boolean authenticateUser(UserDTO userDTO) {
		try {
			logger.info("-----------------authenticateUser---------------------");
			logger.info(userDTO.toString());

			String password = utils.calculateSecretHash(userDTO.getUsername(), userDTO.getPassword(), key);
			UserMosquitto result = userMosquittoDAO.findByUsernameAndPassword(userDTO.getUsername(), password);
			if (result != null && result.getEstado()) {
				return true;
			}

			throw new ExceptionUserAuthentication("User not authorised", HttpStatus.UNAUTHORIZED);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public boolean verifySuperuser(UserDTO userDTO) {
		try {
			logger.info("----------------verifySuperuser----------------------");
			logger.info(userDTO.toString());

			UserMosquitto userMosquitto = userMosquittoDAO.findByUsername(userDTO.getUsername());
			if (userMosquitto != null && userMosquitto.getEstado() && userMosquitto.isSuperuser()) {
				return true;
			}
			throw new ExceptionUserAuthentication("User is not superuser", HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public boolean authorizeUser(AuthorizationAclDTO authorizationAclDTO) {
		try {
			logger.info("----------------authorizeUser----------------------");
			logger.info(authorizationAclDTO.toString());

			AclMosquitto aclMosquitto = aclMosquittoDAO.findByTopicAndUsername(authorizationAclDTO.getTopic(),
					authorizationAclDTO.getUsername());

			if (aclMosquitto != null && aclMosquitto.getEstado()) {

				switch (Integer.parseInt(authorizationAclDTO.getAcc().toString())) {
				case Constantes.CODIGO_ACC_READ:
					if (aclMosquitto.getReadPermission()) {
						return true;
					}
					break;
				case Constantes.CODIGO_ACC_WRITE:
					if (aclMosquitto.getWritePermission()) {
						return true;
					}
					break;
				case Constantes.CODIGO_ACC_READ_WRITE:
					if (aclMosquitto.getReadPermission() && aclMosquitto.getWritePermission()) {
						return true;
					}
					break;
				case Constantes.CODIGO_ACC_SUSCRIPTION:
					if (aclMosquitto.getSuscriptionPermission()) {
						return true;
					}
					break;
				default:
					break;
				}
			}

			throw new ExceptionUserAuthentication("ACL not authorised", HttpStatus.FORBIDDEN);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
}
