package co.edu.ucc.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import co.edu.ucc.entity.AclMosquitto;

@Repository
public interface IAclMosquittoDAO extends JpaRepository<AclMosquitto, Long> {

	@Query(value = "select a from AclMosquitto a where a.topic = :topic and a.userMosquitto.username = :username")
	public AclMosquitto findByTopicAndUsername(@Param("topic") String topic, @Param("username") String username);

}
